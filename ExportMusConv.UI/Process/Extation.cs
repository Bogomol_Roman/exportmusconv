﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ExportMusConv.UI.Process
{
    public static class Extation
    {
        public static string Decode(this string text)
        {
          return  System.Text.RegularExpressions.Regex.Unescape(text);
            
        }

    }
}
