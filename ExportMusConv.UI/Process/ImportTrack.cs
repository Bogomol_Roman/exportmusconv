﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Documents;
using Csv;
using ExportMusConv.UI.Entity.DTO;
using System.Xml;

namespace ExportMusConv.UI.Process
{
    public class ImportTrack
    {

        public List<TrackDTO> LoadCsv(string fileName)
        {
            try
            {
                Stream fileStream = new FileStream(fileName, FileMode.Open);
                List<ICsvLine> lines =
                    CsvReader.ReadFromStream(fileStream).ToList();

                List<TrackDTO> tracksArist = lines
                    .Select(p => new TrackDTO()
                    {
                        Name = p[1],
                        Artist = p[2]
                    }).ToList();
                fileStream.Close();

                return tracksArist;
            }
            catch
            {
                return new List<TrackDTO>();
            }
        }

        public List<TrackDTO> LoadM3U(string fileName)

        {
            try

            {
                List<string> lines = ReadTextLine(fileName);
                bool isFirstrLine = false;
                List<TrackDTO> tracksArist =
                    new List<TrackDTO>();

                foreach (var line in lines)
                {
                    if (!isFirstrLine)
                    {
                        isFirstrLine = true;
                        continue;
                    }
                    else
                    {
                        List<string> temp = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        if (temp.Count == 2)
                        {
                            tracksArist.Add(new TrackDTO()
                            {
                                Name = temp[1].Decode().Replace("?", "")
                            });
                        }


                    }
                }

                return tracksArist;
            }
            catch
            {
                return new List<TrackDTO>();
            }
        }
        public List<TrackDTO> LoadPls(string fileName)
        {
            List<string> lines = ReadTextLine(fileName);
            List<TrackDTO> tracks = new List<TrackDTO>();
            int iter = 0;
            
            
            foreach(string item in lines)
            {
                if (item.StartsWith("t"))
                {
                    if(iter < 9)
                    {
                        tracks.Add(new TrackDTO()
                        {
                            Name = item.Substring(7)
                        });
                    }
                    else if(iter < 99)
                    {
                        tracks.Add(new TrackDTO()
                        {
                            Name = item.Substring(8)
                        });
                    }
                    else if(iter < 999)
                    {
                        tracks.Add(new TrackDTO()
                        {
                            Name = item.Substring(10)
                        });
                    }
                    else if(iter < 9999)
                    {
                        tracks.Add(new TrackDTO()
                        {
                            Name = item.Substring(11)
                        });
                    }
                    else if (iter < 99999)
                    {
                        tracks.Add(new TrackDTO()
                        {
                            Name = item.Substring(12)
                        });
                    }
                    iter++;
                }
                
            }
            return tracks;
        }

        public List<TrackDTO> LoadWpl(string fileName)
        {
            List<TrackDTO> tracks = new List<TrackDTO>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(fileName);
           
            string[] words;

            XmlElement element = xmlDocument.DocumentElement;

            foreach (XmlNode node in element)
            {
                if (node.LastChild.Name == "seq")
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        foreach (XmlNode item in childNode.ChildNodes)
                        {
                            words = item.Attributes[0].Value.Split(new char[] { '\\' });
                            
                            tracks.Add(new TrackDTO()
                            {
                                Name = words[words.Length-1]
                            });
                        }
                    }
                }
            }
            return tracks;
        }

        public List<TrackDTO> LoadXML(string fileName)
        {
            string text = ReadText(fileName);
            Regex reHref = new Regex($"(?<=<key>Name</key><string>)(.*?)(?=</string>)");
            Regex reHrefAr = new Regex($"(?<=<key>Artist</key><string>)(.*?)(?=</string>)");
            List<TrackDTO> tracks = new List<TrackDTO>();

           MatchCollection names = reHref.Matches(text);
            var artist = reHrefAr.Matches(text);

            if (names.Count == artist.Count)
            {
                for (int i = 0; i < names.Count; i++)
                {
                    tracks.Add(new TrackDTO()
                    {
                        Name = names[i].Value,
                        Artist = artist[i].Value

                    });
                }
            }
            else
            {
                foreach (var item in names)
                {
                    tracks.Add(new TrackDTO()
                    {
                        Name = item.ToString()
                    });
                }
            }

            return tracks;
        }

        private List<string> ReadTextLine(string fileName)
        {
            string text = ReadText(fileName);

            List<string> enterTexts = text.Split('\n').ToList();

            return enterTexts;
        }

        private string ReadText(string fileName)
        {
            Stream fileStream = new FileStream(fileName, FileMode.Open);

            using (StreamReader sr = new StreamReader(fileStream, Encoding.UTF8))
            {
                return sr.ReadToEnd();
            }
        }

        public List<TrackDTO> LoadDefault(string fileName)
        {
            try
            {
                List<string> enterTexts = ReadTextLine(fileName);

                List<TrackDTO> tracksArist =
                    new List<TrackDTO>();
                bool isFirstrLine = false;

                foreach (var line in enterTexts)
                {
                    if (!isFirstrLine)
                    {
                        isFirstrLine = true;
                        continue;
                    }
                    else
                    {
                        List<string> temp = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        if (temp.Count == 1)
                        {
                            tracksArist.Add(new TrackDTO()
                            {
                                Name = temp[0]
                            });
                        }
                        else if (temp.Count >= 3)
                        {
                            tracksArist.Add(new TrackDTO()
                            {
                                Name = temp[1],
                                Artist = temp[2]
                            });
                        }

                    }
                }

                return tracksArist;

            }
            catch
            {
                return new List<TrackDTO>();
            }
        }
    }
}
