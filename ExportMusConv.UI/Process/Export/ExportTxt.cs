﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csv;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Export.Abs;

namespace ExportMusConv.UI.Export
{
   public class ExportTxt : IExport
    {
        public Result ExportFile(List<TrackDTO> trackDtos, string path)
        {
            List<string[]> lines = new List<string[]>();
            foreach (var selectedTrack in trackDtos)
            {

                lines.Add(new[] { selectedTrack.Id??"", selectedTrack.Name??"",
                    selectedTrack.Artist??"",
                    selectedTrack.Album??"" });
            }

            try
            {
                int id = 1;

                File.WriteAllText(path,
                    CsvWriter.WriteToText(new[]

                        { "Track ID", "Track Name", "Artist Name", "Album Name" }, lines),
                    Encoding.UTF8);

                return new Result
                {
                    Success = true,
                    Message = "was saved successfully!"
                };
            }
            catch (Exception e)
            {
                return new Result()
                {
                    Message = e.Message
                };
            }
        }
    }
}
