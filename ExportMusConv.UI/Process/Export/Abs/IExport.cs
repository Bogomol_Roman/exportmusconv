﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Entity.DTO;

namespace ExportMusConv.UI.Export.Abs
{
    public interface IExport
    {
        Result ExportFile(List<TrackDTO> trackDtos,string name);
    }
}
