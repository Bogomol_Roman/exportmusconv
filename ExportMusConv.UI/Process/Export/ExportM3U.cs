﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Export.Abs;
using M3U.NET;
using MP3Sharp;

namespace ExportMusConv.UI.Export
{
    public class ExportM3U : IExport
    {
        public Result ExportFile(List<TrackDTO> trackDtos, string name)
        {
            try
            {
                using (StreamWriter stream = new StreamWriter(Path.Combine(name)))
                {
                    foreach (var track in trackDtos)
                    {
                        stream.WriteLine(track.Url);

                    }

                }

                return new Result()
                {
                    Success = true
                };
            }
            catch(Exception e)
            {
                return new Result()
                {
                    Message = e.Message
                };
            }
        }
    }
}
