﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csv;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Export.Abs;
using Newtonsoft.Json;

namespace ExportMusConv.UI.Export
{
    public class ExportJson : IExport
    {
        public Result ExportFile(List<TrackDTO> trackDtos, string path)
        {
            try
            {
                List<string[]> lines = new List<string[]>();
                foreach (var selectedTrack in trackDtos)
                {

                    lines.Add(new[] { selectedTrack.Id??"", selectedTrack.Name??"",
                    selectedTrack.Artist??"",
                    selectedTrack.Album??"" });
                }

                string json = JsonConvert.SerializeObject(lines);

                using (StreamWriter sr = new StreamWriter(path))
                {
                    sr.WriteLine(json);
                }

                return new Result
                {
                    Success = true,
                    Message = "was saved successfully!"
                };
            }
            catch (Exception e)
            {
                return new Result()
                {
                    Message = e.Message
                };
            }

        }
    }
}
