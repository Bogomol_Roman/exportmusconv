﻿using ExportMusConv.UI.Entity.DTO;
using Fizzler.Systems.HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExportMusConv.UI.Process
{
    public class ParseUrl
    {
        public List<TrackDTO> ParseUrlYouTube(string url)
        {
            List<string> hrefs = ReadTextLine(url);
            List<TrackDTO> tracks = new List<TrackDTO>();

            HttpWebRequest request;
            HttpWebResponse response;
            string str;
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            try
            {
                foreach (string href in hrefs)
                {
                    request = (HttpWebRequest)WebRequest.Create(href);
                    response = (HttpWebResponse)request.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            str = streamReader.ReadToEnd();
                            doc.LoadHtml(str);
                            str = doc.DocumentNode.QuerySelector("title").InnerHtml;
                            str = str.Substring(0, str.Length - 10);
                            tracks.Add(new TrackDTO()
                            {
                                Name = str
                            });
                            response.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error");
            }

            return tracks;
        }

        public List<TrackDTO> ParseTidal(string url)
        {
            List<string> hrefs = ReadTextLine(url);
            List<TrackDTO> tracks = new List<TrackDTO>();

            HttpWebRequest request;
            HttpWebResponse response;
            string str;
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            try
            {
                foreach (string href in hrefs)
                {
                    request = (HttpWebRequest)WebRequest.Create(href);
                    response = (HttpWebResponse)request.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            str = streamReader.ReadToEnd();
                            doc.LoadHtml(str);
                            str = doc.DocumentNode.QuerySelector("title").InnerText;
                           
                            tracks.Add(new TrackDTO()
                            {
                                Name = str
                            });
                            response.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error");
            }

            return tracks;
        }
        public List<TrackDTO> ParserSpotify(string url)
        {
            List<string> hrefs = ReadTextLine(url);
            List<TrackDTO> tracks = new List<TrackDTO>();

            HttpWebRequest request;
            HttpWebResponse response;
            string str;
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            try
            {
                foreach (string href in hrefs)
                {
                    request = (HttpWebRequest)WebRequest.Create(href);
                    response = (HttpWebResponse)request.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            str = streamReader.ReadToEnd();
                            doc.LoadHtml(str);
                            str = doc.DocumentNode.QuerySelector("title").InnerHtml;
                            str = str.Substring(0, str.Length-11);
                            tracks.Add(new TrackDTO()
                            {
                                Name = str
                            });
                            response.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error");
            }

            return tracks;
        }


        private List<string> ReadTextLine(string str)
        {
            string text = str;

            List<string> enterTexts = text.Split('\n').ToList();

            return enterTexts;
        }
    }
}
