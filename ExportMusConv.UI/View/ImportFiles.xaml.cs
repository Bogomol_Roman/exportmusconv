﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExportMusConv.UI.ViewModel;

namespace ExportMusConv.UI.View
{
    /// <summary>
    /// Логика взаимодействия для ImportFiles.xaml
    /// </summary>
    public partial class ImportFiles : Window
    {
        public ImportFiles()
        {
            InitializeComponent();
            DataContext = new ImportViewModel();
        }

        private void Label_MouseEnter(object sender, MouseButtonEventArgs e)
        {
          this.Close();
        }
    }
}
