﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExportMusConv.UI.Config;
using ExportMusConv.UI.Config.ApiConfig;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Export;
using ExportMusConv.UI.Itunes;
using ExportMusConv.UI.Process;
using ExportMusConv.UI.ViewModel;
using MessageBox = System.Windows.Forms.MessageBox;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для ServiceItemControl.xaml
    /// </summary>
    public partial class ServiceItemControl : System.Windows.Controls.UserControl
    {
        private ItunseLoginUser _itunseLoginUser;
        private List<TrackDTO> _trackDtos;
        private string _name;
        public static bool IsReload=false;
        public ServiceItemControl(List<TrackDTO> trackDtos,string name)
        {
            InitializeComponent();
            _trackDtos = trackDtos;
            _itunseLoginUser = new ItunseLoginUser();
            _name = name;
            TypeFiles.ItemsSource = ApiServiceHolder.TypeFiles;
            TypeFiles.SelectionChanged += TypeFiles_SelectionChanged;
            ImportViewModel.GetINstanse().Step = "Step 4";
            ImportViewModel.GetINstanse().Description = " : Select a Destination";
        }

   

        private async void TypeFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (TypeFiles.SelectedItem is TypeApiService service)
            {

                if (service.ServiceType == ServiceType.Csv)
                {
                    ExportCsv export = new ExportCsv();
                    using (var folderBrowserDialog = new FolderBrowserDialog())
                    {
                        DialogResult dialogResult = folderBrowserDialog.ShowDialog();
                        if (dialogResult == System.Windows.Forms.DialogResult.OK)
                        {
                        }

                        string fileName = "Tracks";
                        try
                        {
                            int id = 1;
                            while (File.Exists($"{folderBrowserDialog.SelectedPath}\\{fileName}.csv"))
                            {
                                fileName = $"{fileName} ({id})";
                                id++;
                            }

                            string path = System.IO.Path.Combine(folderBrowserDialog.SelectedPath, $"{ fileName}.csv");
                            Result result1 = new Result();

                            result1 = export.ExportFile(_trackDtos.ToList(), path);


                            if (result1.Success)
                            {
                                System.Windows.MessageBox.Show(" was saved successfully!",
                                       "Success!", MessageBoxButton.OK, MessageBoxImage.None);
                            }
                            else
                            {
                                System.Windows.MessageBox.Show(result1.Message,
                                       "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        catch
                        {

                        }

                    }

                    return;
                }

                if (!ApiServiceConfigHoldder.Tokens.ContainsKey(service.ServiceType))
                {
                    MessageBox.Show($"Log in {service.Title}");

                    return;
                }

                LoadFrom loadForm = new LoadFrom();
                bool result = false;
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {

                    loadForm.Show();
                });

                await Task.Run(async () =>
                {
                    switch (service.ServiceType)
                    {
                        case ServiceType.ITunes:
                            result = await _itunseLoginUser.CreatePlaylist(_name,
                                _trackDtos.Where(p => p != null && p.IsCheck).ToList());
                            break;
                        default:
                            MessageBox.Show("Not Implemented");
                            break;
                    }
                });


                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    loadForm.Close();
                    
                    IsReload = result;
                    var s = result ? MessageBox.Show("PlayList success created") : MessageBox.Show("Sorry, some error");
                });

            }
        }
    }
}
