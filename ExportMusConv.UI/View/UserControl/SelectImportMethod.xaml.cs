﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExportMusConv.UI.Config.Enum;
using ExportMusConv.UI.Entity.DTO;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для SelectImportMethod.xaml
    /// </summary>
    public partial class SelectImportMethod : System.Windows.Controls.UserControl
    {
        public SelectImportMethod()
        {
            InitializeComponent();
        }

        private void ListBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            ImportFile importFile = ImaprtFiles.SelectedItem as ImportFile;

            if (importFile == null)
            {
                return;
            }
            switch (importFile.ImportMethodType)
            {
                case ImportMethodType.File:
                    ImportMethodTypeFile();
                    break;
                case ImportMethodType.Plain:
                    ImportMethodTypePlain();
                    break;
                case ImportMethodType.WebURl:
                    ImportMethodTypeWebURl();
                    break;







            }
        }

        private void ImportMethodTypeFile()
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new ImportMethodFile());
        }
        public void ImportMethodTypePlain()
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new ImportMethodPlain());
        }
        public void ImportMethodTypeWebURl()
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new ImportMethodWebURl());
        }
    }
}
