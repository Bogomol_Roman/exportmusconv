﻿using ExportMusConv.UI.Entity.DTO;
using Fizzler.Systems.HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для ImportMethodWebURl.xaml
    /// </summary>
    public partial class ImportMethodWebURl : System.Windows.Controls.UserControl
    {
        private bool _tbUrl;
        private List<TrackDTO> _tracksArist;
        private bool _isTrackCheck = false;
        private bool _isClickText;
        private bool _stateSort;
        public ImportMethodWebURl()
        {
            InitializeComponent();
            
            tbUrl.SelectionChanged += TbUrl_SelectionChanged;
            Position.Click += Position_Click;
            Search.SelectionChanged += Search_SelectionChanged;
            ChachAll.Checked += ChachAll_Checked;
            ChachAll.Unchecked += ChachAll_Unchecked;

            Confirm.Click += Confirm_Click;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new CreatePlaylist(_tracksArist));
        }

        private void TbUrl_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_tbUrl)
            {
                tbUrl.Text = string.Empty;
                tbUrl.Foreground = new SolidColorBrush(Colors.Black);
                _tbUrl = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(tbUrl.Text))
                {
                    CreateUrl.IsEnabled = true;
                }
                else
                {
                    CreateUrl.IsEnabled = false;
                }
            }
        }

        private void CreateUrl_Click(object sender, RoutedEventArgs e)
        {
            _tracksArist = ParseHtml();
            Tracks.ItemsSource = _tracksArist;

            SetContent();
            UrlGrid.Visibility = Visibility.Hidden;
            GridList.Visibility = Visibility.Visible;
            Tracks.Visibility = Visibility.Visible;
            Confirm.Visibility = Visibility.Visible;

        }

        public  List<TrackDTO> ParseHtml()
        {
            
            List<TrackDTO> tracks = new List<TrackDTO>();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(tbUrl.Text);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        string str = streamReader.ReadToEnd();
                        doc.LoadHtml(str);
                        str = doc.DocumentNode.QuerySelector("title").InnerHtml;
                        str = str.Substring(0, str.Length - 10);
                        tracks.Add(new TrackDTO()
                        {
                            Name = str
                        });
                        response.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Invalid parameters");
            }
           

           
            return tracks;
        }

        private List<string> ReadTextLine()
        {
            string text = tbUrl.Text;

            List<string> enterTexts = text.Split('\n').ToList();

            return enterTexts;
        }




        #region 
        private void ChachAll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isTrackCheck)
            {
                _isTrackCheck = false;
                return;

            }
            _tracksArist.ForEach(p => p.IsCheck = false);
            SelectAll.Content = "Select all";
            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void ChachAll_Checked(object sender, RoutedEventArgs e)
        {
            _tracksArist.ForEach(p => p.IsCheck = true);
            SelectAll.Content = "Unselect all";

            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void Search_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_isClickText)
            {
                Search.Text = string.Empty;
                _isClickText = true;
            }
            else
            {
                Tracks.ItemsSource = _tracksArist.Where(p => p.Name.ToLower()
                    .Contains(Search.Text));
            }
        }

        private void Position_Click(object sender, RoutedEventArgs e)
        {
            if (_stateSort)
            {
                _tracksArist = _tracksArist.OrderBy(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↓", "↑");
                _stateSort = false;

            }
            else
            {
                _tracksArist = _tracksArist.OrderByDescending(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↑", "↓");
                _stateSort = true;
            }
        }

        private void Tracks_MouseMove(object sender, MouseEventArgs e)
        {
            SetContent();
        }

        private void Tracks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Tracks.SelectedItems is TrackDTO)
            {
                SetContent();
            }
        }

        private void SetContent()
        {
            var splits = ItemSelectText.Text.Split(' ', '/');
            ItemSelectText.Text = $"{_tracksArist.Count(p => p.IsCheck)}/{_tracksArist.Count} {splits[2]} {splits[3]}";
        }

        private void Track_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is TrackDTO track)
            {
                if (!track.IsCheck)
                {
                    SelectAll.Content = "Select all";
                    _isTrackCheck = true;
                    ChachAll.IsChecked = false;
                }
                else if (_tracksArist.All(p => p.IsCheck))
                {
                    SelectAll.Content = "Unselect all";
                    _isTrackCheck = false;
                    ChachAll.IsChecked = true;
                }


            }
        }
        

        private void InitProperty()
        {
            foreach (var track in _tracksArist)
            {
                track.PropertyChanged += Track_PropertyChanged;
            }
        }
        #endregion


    }
}
