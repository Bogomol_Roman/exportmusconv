﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Csv;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Process;
using ExportMusConv.UI.ViewModel;
using DataFormats = System.Windows.DataFormats;
using DragEventArgs = System.Windows.DragEventArgs;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для ImportMethodFile.xaml
    /// </summary>
    public partial class ImportMethodFile : System.Windows.Controls.UserControl
    {
        private List<TrackDTO> _tracksArist;
        private ImportTrack _impoerTrack;
        private bool _isClickText;
        private bool _stateSort;
        public ImportMethodFile()
        {
            InitializeComponent();
            Confirm.Click += Confirm_Click;
            Search.SelectionChanged += Search_SelectionChanged; ;
            _impoerTrack = new ImportTrack();
            Position.Click += Position_Click;
            Tracks.SelectionChanged += Tracks_SelectionChanged;
            Tracks.MouseMove += Tracks_MouseMove;

            ChachAll.Unchecked += ChachAll_Unchecked;
            ChachAll.Checked += ChachAll_Checked;

            ImportViewModel.GetINstanse().Step = "Step 1.1";
            ImportViewModel.GetINstanse().Description = " : Select your playlist file";
        }

        private void ChachAll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isTrackCheck)
            {
                _isTrackCheck = false;
                return;

            }
            _tracksArist.ForEach(p => p.IsCheck = false);
            SelectAll.Content = "Unselect all";

            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void ChachAll_Checked(object sender, RoutedEventArgs e)
        {
           
            _tracksArist.ForEach(p => p.IsCheck = true);
            SelectAll.Content = "Select all";

            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void Tracks_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SetContent();
        }
        private void Tracks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Tracks.SelectedItems is TrackDTO)
            {
                SetContent();
            }
        }

        private void SetContent()
        {
            var splits = ItemSelectText.Text.Split(' ', '/');
            ItemSelectText.Text = $"{_tracksArist.Count(p => p.IsCheck)}/{_tracksArist.Count} {splits[2]} {splits[3]}";
        }

        private void Position_Click(object sender, RoutedEventArgs e)
        {
            if (_stateSort)
            {
                _tracksArist = _tracksArist.OrderBy(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↓", "↑");
                _stateSort = false;

            }
            else
            {
                _tracksArist = _tracksArist.OrderByDescending(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↑", "↓");
                _stateSort = true;
            }
        }

        private void Search_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_isClickText)
            {
                Search.Text = string.Empty;
                _isClickText = true;
            }
            else
            {
                Tracks.ItemsSource = _tracksArist.Where(p => p.Name.ToLower()
                    .Contains(Search.Text));
            }
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new CreatePlaylist(_tracksArist));
        }

        private async void ImagePanel_Drop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                ParserFile(files);
            }
        }

        private async void ParserFile(string[] files)
        {
            _tracksArist =
                new List<TrackDTO>();
            LoadFrom loadForm = new LoadFrom();

            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {

                loadForm.Show();
            });


            foreach (var file in files)
            {
                await Task.Run(() =>
                {
                    switch (System.IO.Path.GetExtension(file))
                    {
                        case ".csv":
                            _tracksArist.AddRange(_impoerTrack.LoadCsv(file));
                            break;
                        case ".m3u":
                            _tracksArist.AddRange(_impoerTrack.LoadM3U(file));
                            break;
                        case ".m3u8":
                            _tracksArist.AddRange(_impoerTrack.LoadM3U(file));
                            break;
                        case ".xml":
                            _tracksArist.AddRange(_impoerTrack.LoadXML(file));
                            break;
                        case ".wpl":
                            _tracksArist.AddRange(_impoerTrack.LoadWpl(file));
                            break;
                        case ".pls":
                            _tracksArist.AddRange(_impoerTrack.LoadPls(file));
                            break;
                        default:
                            _tracksArist.AddRange(_impoerTrack.LoadDefault(file));
                            break;
                    }
                });
                // mainViewModel.LoadFile(file);
            }

            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                GridList.Visibility = Visibility.Visible;
                Tracks.ItemsSource = _tracksArist;
                InitProperty();
                ImportViewModel.GetINstanse().Step = "Step 2";
                ImportViewModel.GetINstanse().Description = " : Confirm the tracklist";
                SetContent();
                SelectFile.Visibility = Visibility.Hidden;
                Tracks.Visibility = Visibility.Visible;
                Confirm.Visibility = Visibility.Visible;
                loadForm.Close();
            });
        }

        private void InitProperty()
        {
            foreach (var track in _tracksArist)
            {
                track.PropertyChanged += Track_PropertyChanged;
            }
        }

        private bool _isTrackCheck = false;

        private void Track_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is TrackDTO track)
            {
                if (!track.IsCheck)
                {
                    SelectAll.Content = "Select all";
                    _isTrackCheck = true;
                    ChachAll.IsChecked = false;
                }
                else if (_tracksArist.All(p => p.IsCheck))
                {
                    SelectAll.Content = "Unselect all";
                    _isTrackCheck = false;
                    ChachAll.IsChecked = true;
                }

              
            }
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                    ParserFile(new[] { filePath });
                }
            }
        }
    }
}
