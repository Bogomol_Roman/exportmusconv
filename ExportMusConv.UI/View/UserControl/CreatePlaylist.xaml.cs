﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Itunes;
using ExportMusConv.UI.ViewModel;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для CreatePlaylist.xaml
    /// </summary>
    public partial class CreatePlaylist : System.Windows.Controls.UserControl
    {
        private List<TrackDTO> _trackDtos;
        private ItunseLoginUser _itunseLoginUser;
        private bool _titlePlayList;
        private bool _description;
        public CreatePlaylist(List<TrackDTO> trackDtos)
        {
            InitializeComponent();
            _trackDtos = trackDtos;
            _itunseLoginUser = new ItunseLoginUser();
            Create.Click += Create_Click;
            Description.SelectionChanged += Description_SelectionChanged;
            TitlePlayList.SelectionChanged += TitlePlayList_SelectionChanged;
            ImportViewModel.GetINstanse().Step = "Step 3";
            ImportViewModel.GetINstanse().Description = " : Configure your Playlist";

        }

        private void Description_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_description)
            {
                Description.Text = string.Empty;
                Description.Foreground = new SolidColorBrush(Colors.Black);
                _description = true;
            }
        }

        private void TitlePlayList_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_titlePlayList)
            {
                TitlePlayList.Text = string.Empty;
                TitlePlayList.Foreground = new SolidColorBrush(Colors.Black);
                _titlePlayList = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(TitlePlayList.Text))
                {
                    Create.IsEnabled = true;
                }
                else
                {
                    Create.IsEnabled = false;
                }
            }
        }

        private async void Create_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TitlePlayList.Text))
            {
                MessageBox.Show("title is empty");

                return;
            }
            MainGrid.Children.Clear();
            
            MainGrid.Children.Add(new ServiceItemControl(_trackDtos, TitlePlayList.Text));
        }
    }
}
