﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExportMusConv.UI.Config.Holder;
using ExportMusConv.UI.Entity.DTO;
using System.Net;
using System.IO;
using Fizzler.Systems.HtmlAgilityPack;
using ExportMusConv.UI.Process;

namespace ExportMusConv.UI.View.UserControl
{
    /// <summary>
    /// Логика взаимодействия для ImportMethodPlain.xaml
    /// </summary>
    public partial class ImportMethodPlain : System.Windows.Controls.UserControl
    {
        public bool _listTrack = true;
        private List<TrackDTO> _tracksArist;
        private bool _isClickText;
        private bool _stateSort;
        private bool _isTrackCheck = false;
        public ImportMethodPlain()
        {
            InitializeComponent();
            ListTrack.Text = ImportMethodPlainHolder.InputInfo;
            Create.Click += Create_Click;
            ListTrack.SelectionChanged += ListTrack_SelectionChanged;

            Confirm.Click += Confirm_Click;
            Tracks.SelectionChanged += Tracks_SelectionChanged;
            Tracks.MouseMove += Tracks_MouseMove;
            Position.Click += Position_Click;
            Search.SelectionChanged += Search_SelectionChanged;
            ChachAll.Checked += ChachAll_Checked;
            ChachAll.Unchecked += ChachAll_Unchecked;
        }

        private void ChachAll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_isTrackCheck)
            {
                _isTrackCheck = false;
                return;

            }
            _tracksArist.ForEach(p => p.IsCheck = false);
            SelectAll.Content = "Select all";
            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void ChachAll_Checked(object sender, RoutedEventArgs e)
        {
            _tracksArist.ForEach(p => p.IsCheck = true);
            SelectAll.Content = "Unselect all";

            Tracks.ItemsSource = null;

            Tracks.ItemsSource = _tracksArist;
            SetContent();
        }

        private void Search_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!_isClickText)
            {
                Search.Text = string.Empty;
                _isClickText = true;
            }
            else
            {
                Tracks.ItemsSource = _tracksArist.Where(p => p.Name.ToLower()
                    .Contains(Search.Text));
            }
        }

        private void Position_Click(object sender, RoutedEventArgs e)
        {
            if (_stateSort)
            {
                _tracksArist = _tracksArist.OrderBy(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↓", "↑");
                _stateSort = false;

            }
            else
            {
                _tracksArist = _tracksArist.OrderByDescending(p => p.Name).ToList();
                Tracks.ItemsSource = _tracksArist;
                Position.Content = Position.Content.ToString().Replace("↑", "↓");
                _stateSort = true;
            }
        }

        private void Tracks_MouseMove(object sender, MouseEventArgs e)
        {
            SetContent();
        }

        private void Tracks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Tracks.SelectedItems is TrackDTO)
            {
                SetContent();
            }
        }

        private void SetContent()
        {
            var splits = ItemSelectText.Text.Split(' ', '/');
            ItemSelectText.Text = $"{_tracksArist.Count(p => p.IsCheck)}/{_tracksArist.Count} {splits[2]} {splits[3]}";
        }

        private void Track_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is TrackDTO track)
            {
                if (!track.IsCheck)
                {
                    SelectAll.Content = "Select all";
                    _isTrackCheck = true;
                    ChachAll.IsChecked = false;
                }
                else if (_tracksArist.All(p => p.IsCheck))
                {
                    SelectAll.Content = "Unselect all";
                    _isTrackCheck = false;
                    ChachAll.IsChecked = true;
                }


            }
        }

        private void InitProperty()
        {
            foreach (var track in _tracksArist)
            {
                track.PropertyChanged += Track_PropertyChanged;
            }
        }

        private void ListTrack_SelectionChanged(object sender, RoutedEventArgs e)
        {

            if (_listTrack)
            {
                ListTrack.Text = string.Empty;
                ListTrack.Foreground = new SolidColorBrush(Colors.Black);
                _listTrack = false;
            }
            else
            {
                if (string.IsNullOrEmpty(ListTrack.Text))
                {
                    Create.IsEnabled = false;
                }
                else
                {
                    Create.IsEnabled = true;
                }
            }
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new CreatePlaylist(_tracksArist));
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            ParseUrl parse = new ParseUrl();
            if (ListTrack.Text.StartsWith("https://www.youtube.com"))
            {
                _tracksArist = parse.ParseUrlYouTube(ListTrack.Text);
            }
            else if(ListTrack.Text.StartsWith("https://open.spotify.com"))
            {
                _tracksArist = parse.ParserSpotify(ListTrack.Text);
            }
            else if (ListTrack.Text.StartsWith("https://listen.tidal.com"))
            {
                _tracksArist = parse.ParseTidal(ListTrack.Text);
            }
            else
            {
                _tracksArist = ParseTrack();
            }
           
            Tracks.ItemsSource = _tracksArist;

            GridList.Visibility = Visibility.Visible;
            Tracks.Visibility = Visibility.Visible;
            Confirm.Visibility = Visibility.Visible;
            PlainGrid.Visibility = Visibility.Hidden;
            InitProperty();
            
            //MainGrid.Children.Clear();
           // MainGrid.Children.Add(new ExportMusConv.UI.View.UserControl.ImpoortMethodPlainList(_tracksArist));
        }

       
        
        public List<TrackDTO> ParseHtmlTrack()
        {
            List<string> hrefs = ReadTextLine();
            List<TrackDTO> tracks = new List<TrackDTO>();

            HttpWebRequest request;
            HttpWebResponse response;
            string str;
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            try
            {
                foreach (string href in hrefs)
                {
                    request = (HttpWebRequest)WebRequest.Create(href);
                    response = (HttpWebResponse)request.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            str = streamReader.ReadToEnd();
                            doc.LoadHtml(str);
                            str = doc.DocumentNode.QuerySelector("title").InnerHtml;
                            str = str.Substring(0, str.Length - 10);
                            tracks.Add(new TrackDTO()
                            {
                                Name = str
                            });
                            response.Close();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error");
            }

            return tracks;
        }

        

        

        public List<TrackDTO> ParseTrack()
        {
            try
            {
                List<string> enterTexts = ReadTextLine();

                List<TrackDTO> tracksArist =
                    new List<TrackDTO>();

                foreach (var line in enterTexts)
                {
                    if (line != "\r" )
                    {

                        tracksArist.Add(new TrackDTO()
                        {
                            Name = line
                        });
                    }
                }

                return tracksArist;

            }
            catch
            {
                return new List<TrackDTO>();
            }
        }

        private List<string> ReadTextLine()
        {
            string text = ListTrack.Text;

            List<string> enterTexts = text.Split('\n').ToList();

            return enterTexts;
        }

        
    }
}
