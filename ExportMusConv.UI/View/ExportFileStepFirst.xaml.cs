﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExportMusConv.UI.Config;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.ViewModel;

namespace ExportMusConv.UI.View
{
    /// <summary>
    /// Логика взаимодействия для ExportFileStepFirst.xaml
    /// </summary>
    public partial class ExportFileStepFirst : System.Windows.Controls.UserControl
    {
        public ExportFileStepFirst()
        {
            InitializeComponent();
            TypeFiles.SelectionChanged += TypeFiles_SelectionChanged;
        }

        private void TypeFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TypeFiles.Visibility = Visibility.Hidden;
            Tracks.Visibility = Visibility.Visible;
            Export.Visibility = Visibility.Visible;

            if (TypeFiles.SelectedItem is TypeFile itemData)
            {
                TypeFileHolder.CurrentFileType = itemData.FileType;

                foreach (var playList in MainViewModel.GetInstanse().AllPlaylists)
                {
                    if (!playList.IsSelectedItem)
                    {
                        continue;
                    }

                    foreach (var playListTrack in playList.Tracks)
                    {
                        MainViewModel.GetInstanse().Tracks.Add(playListTrack);
                    }
                }

            }
        }
    }
}
