﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExportMusConv.UI.View
{
    /// <summary>
    /// Логика взаимодействия для LoadFrom.xaml
    /// </summary>
    public partial class LoadFrom : Window
    {
        private bool _isShow=false;
        public LoadFrom()
        {
            InitializeComponent();
        }
        public LoadFrom(string name)
        {
            InitializeComponent();
            TextBlock.Text = name;
        }

        public void ShowFrom()
        {
            if (!_isShow)
            {
                this.Show();
                _isShow = true;
            }
        }
    }
}
