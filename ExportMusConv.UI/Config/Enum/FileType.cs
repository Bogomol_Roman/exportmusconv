﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportMusConv.UI.Config
{
    public enum FileType
    {
        Csv,
        Xml,
        Json,
        Txt,
        M3u,
        M3u8,
        Wpl,
        Xspf
    }
}
