﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportMusConv.UI.Config
{
   public static class ViewConfig
    {
        public static double DeltaWidth { get; set; } = 50;
    }
}
