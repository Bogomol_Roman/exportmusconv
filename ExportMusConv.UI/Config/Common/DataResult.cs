﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportMusConv.UI.Config.Common
{
    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
    public class DataResult<T> :Result
    {
        public T Data { get; set; }
    }
}
