﻿using ExportMusConv.UI.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ExportMusConv.UI.Config
{
    public static class TypeFileHolder
    {
        public static List<TypeFile> TypeFiles { get; set; }

        public static FileType CurrentFileType { get; set; }

        static TypeFileHolder()
        {
            TypeFiles = new List<TypeFile>() {
            new TypeFile(){ Title="Csv",FileType= FileType.Csv, ImageData=LoadImage("txt.png")},
             new TypeFile(){ Title="Xml",FileType= FileType.Xml, ImageData=LoadImage("xml.png") },
              new TypeFile(){ Title="Json",FileType= FileType.Json, ImageData=LoadImage("xml.png") },
            new TypeFile() { Title = "Txt", FileType = FileType.Txt, ImageData = LoadImage("txt.png") },
             new TypeFile() { Title = "M3u", FileType = FileType.M3u, ImageData = LoadImage("xml.png") },
              new TypeFile() { Title = "M3u8", FileType = FileType.M3u8, ImageData = LoadImage("xml.png") },
                 new TypeFile() { Title = "wpl", FileType = FileType.Wpl, ImageData = LoadImage("xml.png") },
              new TypeFile() { Title = "xspf", FileType = FileType.Xspf, ImageData = LoadImage("xml.png") }
            };
        }

        private static BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/Resources/" + filename));
        }
    }
}
