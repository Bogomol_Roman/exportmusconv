﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ExportMusConv.UI.Config.Enum;
using ExportMusConv.UI.Entity.DTO;

namespace ExportMusConv.UI.Config.Holder
{
    public static class ImportMethodHolder
    {
        public static List<ImportFile> ImportFiles { get; set; }= new List<ImportFile>();

        static ImportMethodHolder()
        {
            ImportFiles.Add(new ImportFile()
            {
                Name = "FROM FILE",
                Description = "Import a playlist from a playlist file (.m3u, .txt, .csv, .xspf and more)",
                ImportMethodType =  ImportMethodType.File,
                ImageData = LoadImage("files.png")
            });

            ImportFiles.Add(new ImportFile()
            {
                Name = "FROM PLAIN TEXT",
                Description = "Import a playlist from a Plain Text ",
                ImportMethodType = ImportMethodType.Plain,
                ImageData = LoadImage("plainText.png")
            });

            ImportFiles.Add(new ImportFile()
            {
                Name = "FROM WEB URL",
                Description = "Import a playlist from a  Web Url",
                ImportMethodType = ImportMethodType.WebURl,
                ImageData = LoadImage("webURL.png")
                
            });
        }

        private static BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/Resources/" + filename));
        }
    }
}
