﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExportMusConv.UI.Config.Holder
{
    public static class ImportMethodPlainHolder
    {
        public static string InputInfo { get; set; } = "Type or paste tracks " +
            "here, one per line (Title - Artist or track Url). \n \n" +
            "Example : \n" +
            "No Tears Left To Cry - Ariana Grange \n" +
            "https://open.spotify.com/track/6ywvpOWw6GerSxaY8WQI2 \n" +
            "Africa - Toto";
       
       
    }
}
