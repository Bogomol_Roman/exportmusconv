﻿using ExportMusConv.UI.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ExportMusConv.UI.Config
{
    public static class ApiServiceHolder
    {
        public static List<TypeApiService> TypeFiles { get; set; }

        static ApiServiceHolder()
        {
            TypeFiles = new List<TypeApiService>();
            TypeFiles.Add(new TypeApiService()
            {
                ServiceType = ServiceType.ITunes,
                Title = "ITunes",
                ImageData = LoadImage("iconfinder_itunes.png")
            });
            TypeFiles.Add(new TypeApiService()
            {
                ServiceType = ServiceType.Napster,
                Title = "Napster",
                ImageData = LoadImage("napster.png")
            });
            TypeFiles.Add(new TypeApiService()
            {
                ServiceType = ServiceType.Csv,
                Title = "Csv",
                ImageData = LoadImage("PlainPhoto.png")
            });
        }

        private static BitmapImage LoadImage(string filename)
        {

            return new BitmapImage(new Uri(@"pack://application:,,,/Resources/" + filename));
        }
    }
}
