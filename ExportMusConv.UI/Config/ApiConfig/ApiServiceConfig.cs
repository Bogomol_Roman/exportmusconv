﻿using System.Collections.Generic;
using System.Linq;
using System.Net;


namespace ExportMusConv.UI.Config.ApiConfig
{
    public class ApiServiceConfig
    {
        public string BaseUrl { get; set; }
        public ServiceType ServiceType { get; set; }

        public  UserData UserData { get; set; } = new UserData();
        public string Token { get; set; }

        public Client Client { get; set; }
        public ApiServiceData<Dictionary<string, string>> Parametrs { get; set; }
        public ApiServiceData<Dictionary<string, string>> Headers { get; set; }
    }

    public class Client
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string CallbackUri { get; set; }
    }

    public class ApiServiceData<T> where T : new()
    {
        public T Data { get; set; } = new T();
    }

    public class UserData
    {
        public CookieContainer CookieContainer { get; set; } = new CookieContainer();
        public string Token { get; set; }
        public string Id { get; set; }
    }

    public static class ApiServiceConfigHoldder
    {
        private static List<ApiServiceConfig> _apiServiceConfigs = new List<ApiServiceConfig>();
        public static  Dictionary<ServiceType,string> Tokens = new Dictionary<ServiceType, string>();
         
        static ApiServiceConfigHoldder()
        {
            InitItunes();
            InitNapster();
        }
        private static void InitItunes()
        {
            _apiServiceConfigs.Add(new ApiServiceConfig()
            {
                BaseUrl = $"https://buy.itunes.apple.com/commerce/account/authenticateMusicKitRequest",
                ServiceType = ServiceType.ITunes,
                Token =
                    "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkI0RzVITjYyODcifQ.eyJpYXQiOjE1NDE1NTE3NzAsImV4cCI6MTU1NzEwMzc3MCwiaXNzIjoiNDhVTTlUODcyMyJ9.Qp_vs_HmcS9HXUdPDmnPvBFS3rPcJliE-T3XpbMj9zeOq8Lso8UuhphwF7pqzWxabYg4SmRQXpWO34ILA-hhEQ",
                Parametrs = new ApiServiceData<Dictionary<string, string>>()
                {
                    Data = new Dictionary<string, string>
                    {
                        {
                            "jwtToken",
                            "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkI0RzVITjYyODcifQ.eyJpYXQiOjE1NDE1NTE3NzAsImV4cCI6MTU1NzEwMzc3MCwiaXNzIjoiNDhVTTlUODcyMyJ9.Qp_vs_HmcS9HXUdPDmnPvBFS3rPcJliE-T3XpbMj9zeOq8Lso8UuhphwF7pqzWxabYg4SmRQXpWO34ILA-hhEQ"
                        },
                        {"isWebPlayer", "true"},
                        {"LogoURL", "true"}
                    },
                },
                Headers = new ApiServiceData<Dictionary<string, string>>()
                {
                    Data = new Dictionary<string, string>
                {
                    {"Accept-Encoding", "gzip, deflate, br"}
                }
                }
            });
        }

        private static void InitNapster()
        {
            Client client = new Client()
            {
                CallbackUri = "https://developer.napster.com/jsfiddle_proxy",
                ClientSecret = "OTE4ZjhmZDUtYzMwZS00OGI4LWJkYTktODM1NzBiY2EwZDdl",
                ClientId = "NTFkMWQ5YmUtYzAyMi00MjNhLWI3NzEtYWRhMGYyYzcxZDhl"
            };
            _apiServiceConfigs.Add(new ApiServiceConfig()
                {
                    BaseUrl = $"https://api.napster.com/oauth/authorize?client_id={client.ClientId}&redirect_uri=https://musconv.com/&amp&response_type=code",
                    Client = client,
                    ServiceType = ServiceType.Napster
            }
            );
        }

        public static ApiServiceConfig GetApiServiceConfig(ServiceType serviceType)
        {
            return _apiServiceConfigs.FirstOrDefault(p => p.ServiceType == serviceType);
        }
    }
}
