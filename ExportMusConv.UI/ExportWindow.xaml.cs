﻿using ExportMusConv.UI.Config;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExportMusConv.UI
{
    /// <summary>
    /// Логика взаимодействия для ExportWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        private MainViewModel _mainViewModel;

        public ExportWindow(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
            InitializeComponent();
            DataContext = mainViewModel;
            mainViewModel.TypeFiles = new ObservableCollection<TypeFile>
                 (TypeFileHolder.TypeFiles);
        }

        private void Label_MouseEnter(object sender, MouseEventArgs e)
        {
            _mainViewModel.Tracks.Clear();
            this.Close();
        }
    }
}
