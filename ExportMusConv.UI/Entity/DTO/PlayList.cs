﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExportMusConv.UI.Entity.DTO
{
   public class PlayListDTO : BaseDTO
    {
        public double WidthPl { get; set; } = ((System.Windows.Controls.Panel)
            Application.Current.MainWindow.Content).ActualWidth-100;

        private List<TrackDTO> _tracks;
        public List<TrackDTO> Tracks
        {
            get
            {
                if (_tracks == null) _tracks = new List<TrackDTO>();
                return _tracks;
            }
            set
            {
                _tracks = value;
            }
        }
    }
}
