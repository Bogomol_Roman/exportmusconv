﻿using ExportMusConv.UI.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ExportMusConv.UI.ViewModel;

namespace ExportMusConv.UI.Entity.DTO
{
    public class TypeFile : BasePropertyChanged
    {
        public FileType FileType { get; set; }
        public string Title { get; set; }
        public BitmapImage ImageData { get; set; }

        private bool _isSelectedItem;
        public bool IsSelectedItem
        {
            get { return _isSelectedItem; }
            set
            {
                _isSelectedItem = value;
                OnPropertyChanged(nameof(IsSelectedItem));
            }
        }

    }
}
