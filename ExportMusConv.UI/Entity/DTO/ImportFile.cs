﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ExportMusConv.UI.Config.Enum;

namespace ExportMusConv.UI.Entity.DTO
{
   public  class ImportFile
    {
        public string Name { get; set; }
        public BitmapImage ImageData { get; set; }

        public string Description { get; set; }

        public ImportMethodType ImportMethodType { get; set; }
    }
}
