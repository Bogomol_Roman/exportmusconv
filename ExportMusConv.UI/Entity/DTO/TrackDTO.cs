﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportMusConv.UI.Entity.DTO
{
   public class TrackDTO : BaseDTO
    {
        public TrackDTO()
        {
            IsCheck = true;
        }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Duration { get; set; }
        public string Url { get; set; }

        private bool _isCheck;
        public bool IsCheck
        {
            get { return _isCheck; }
            set
            {
                _isCheck = value;
                OnPropertyChanged(nameof(IsCheck));
            }
        }
        public override string ToString()
        {
            return $"{Artist} - {Name} - {Duration}";
        }
    }
}
