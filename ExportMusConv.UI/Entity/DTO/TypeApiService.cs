﻿using ExportMusConv.UI.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ExportMusConv.UI.Entity.DTO
{
   public class TypeApiService
    {
        public ServiceType ServiceType { get; set; }
        public BitmapImage ImageData { get; set; }
        public string Title { get; set; }

        public bool IsLogin { get; set; } = false;
    }
}
