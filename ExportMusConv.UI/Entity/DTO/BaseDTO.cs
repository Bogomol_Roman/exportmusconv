﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExportMusConv.UI.ViewModel;

namespace ExportMusConv.UI.Entity.DTO
{
    public class BaseDTO : BasePropertyChanged
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Visibility IsVisible { get; set; } = Visibility.Hidden;
        private bool _isSelectedItem;
        public bool IsSelectedItem
        {
            get { return _isSelectedItem; }
            set
            {
                _isSelectedItem = value;
                OnPropertyChanged(nameof(IsSelectedItem));
            }
        }

        public override string ToString()
        {
            return $"{Name}";

        }
    }
}
