﻿using ExportMusConv.UI.Config;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;
using ExportMusConv.UI.Api;
using ExportMusConv.UI.Api.Napster;
using ExportMusConv.UI.Config.ApiConfig;
using ExportMusConv.UI.Itunes;
using ExportMusConv.UI.Itunes.Josn;
using ExportMusConv.UI.Process;
using ExportMusConv.UI.View;
using Musconv.Entity.Napster;
using Newtonsoft.Json;
using Cookie = CefSharp.Cookie;

namespace ExportMusConv.UI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CookieVisitor _cookieVisitor;
        private MainViewModel _mainViewModel;
        private ItunseLoginUser _itunseLoginUser;
        private NapsterRepository _napsterRepository;
        private ApiServiceConfig _apiServ;
        public MainWindow()
        {
            InitializeComponent();
            _mainViewModel = new MainViewModel();
            _napsterRepository = new NapsterRepository();
            _itunseLoginUser = new ItunseLoginUser();
            DataContext = _mainViewModel;
            _cookieVisitor = new CookieVisitor();
            this.Activated += MainWindow_Activated;
            this.SizeChanged += MainWindow_SizeChanged;
            CollectionServices.SelectionChanged += CollectionServices_SelectionChanged;
            MyWebBrowser.FrameLoadStart += MyWebBrowser_FrameLoadStart;
            MyWebBrowser.FrameLoadEnd += MyWebBrowser_FrameLoadEnd;

        }

        private void MyWebBrowser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            CefSharp.Cef.GetGlobalCookieManager().VisitAllCookies(_cookieVisitor);

            if (_cookieVisitor == null)
            {
                return;
            }

            Dictionary<string, System.Net.Cookie> cookies = _cookieVisitor.AllCookies;
        }

        private void MainWindow_Activated(object sender, EventArgs e)
        {

            List<PlayListDTO> playLists = new List<PlayListDTO>();

            foreach (var item in _mainViewModel.AllPlaylists)
            {
                item.WidthPl = Width - ViewConfig.DeltaWidth;
                playLists.Add(item);
            }

            _mainViewModel.AllPlaylists = new ObservableCollection<PlayListDTO>(playLists);
        }

        private bool isLogin = false;
        private async void MyWebBrowser_FrameLoadStart(object sender, CefSharp.FrameLoadStartEventArgs e)
        {
            try
            {
                switch (_apiServ.ServiceType)
                {
                    case ServiceType.ITunes:
                        Tunes(e);
                        break;
                    case ServiceType.Napster:
                        Napset(e);
                        break;
                }
            }
            catch { }

        }

        private LoadFrom loadFrom=null;

        private async void Tunes(FrameLoadStartEventArgs e)
        {
            ApiServiceConfig apiServ = ApiServiceConfigHoldder.GetApiServiceConfig
            (ServiceType.ITunes);

            this.Dispatcher.Invoke(() =>
            {
                if (loadFrom == null)
                {
                    loadFrom = new LoadFrom("Autorization ITunes...");
                }
            });

            if (isLogin)
            {
                this.Dispatcher.Invoke(() =>
                {  
                    MyWebBrowser.Visibility = Visibility.Hidden;
                    AllPlaylists.Visibility = Visibility.Visible;
                    loadFrom.ShowFrom();
                });
             
                string res = await GetHTMLFromWebBrowser();
                if (res == null)
                {
                    res = await GetHTMLFromWebBrowser();
                }
                if (res != null)
                {
                    if (res.Contains("musicUserToken"))
                    {

                        Regex reHref = new Regex($"(?<=musicUserToken\":\")(.*?)(?=\")");
                        string token = reHref.Matches(res)[0].Value.Decode();
                        apiServ.UserData.Token = token;

                        _apiServ.UserData.Token = token;
                        ApiServiceConfigHoldder.Tokens.Add(ServiceType.ITunes, token);
                        ApplePlayListInfo playListInfos = await _itunseLoginUser.GetPlayLists();
                        

                        this.Dispatcher.Invoke(() =>
                        {
                            loadFrom.Close();
                            MyWebBrowser.Visibility = Visibility.Hidden;
                            AllPlaylists.Visibility = Visibility.Visible;

                           foreach (var p in playListInfos.Data)
                            {
                                _mainViewModel.AllPlaylists.Add(new PlayListDTO()
                                {
                                    Id = p.Id,
                                    Name = p.Attributes.Name,
                                });
                            }
                        });

                    }
                    else
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            MyWebBrowser.Reload();
                        });
                    }
                }
            }
            else if (!isLogin && e.Frame.IsMain)
            {

                if (e.Frame.Url.Contains("liteSessionId="))
                {
                    Regex reHref = new Regex(@"(?<=liteSessionId=)([^&]*)(?=&)");

                    string value = reHref.Matches(e.Frame.Url)[0].Value;

                    apiServ.UserData.Id = value;
                    this.Dispatcher.Invoke(() =>
                    {
                        MyWebBrowser.Address =
                            $"https://buy.itunes.apple.com/commerce/account/getMusicUserToken?liteSessionId={value}&jwtToken={apiServ.Token}";
                    });

                    isLogin = true;
                }

            }
        }

        private async void Napset(FrameLoadStartEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                Uri browserURL = new Uri(e.Frame.Url);

                if (browserURL.Host.Contains("musconv"))
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        MyWebBrowser.Visibility = Visibility.Hidden;
                    });
                    var code = HttpUtility.ParseQueryString(browserURL.Query).Get("code").Split('&')[0];

                    var values = new Dictionary<string, string>
                    {
                        ["client_id"] = _apiServ.Client.ClientId,
                        ["client_secret"] = _apiServ.Client.ClientSecret,
                        ["response_type"] = "code",
                        ["code"] = code,
                        ["grant_type"] = "authorization_code",
                        ["redirect_uri"] = "https%3A%2F%2Fwww.musconv.com"
                    };

                    var response = await new HttpClient()
                        .PostAsync("https://api.napster.com/oauth/access_token",
                            new FormUrlEncodedContent(values));

                    var responseStream = await response.Content.ReadAsStreamAsync();
                    _napsterRepository.SetAcces(responseStream);

                    this.Dispatcher.Invoke(() =>
                    {
                        MyWebBrowser.Visibility = Visibility.Hidden;
                        AllPlaylists.Visibility = Visibility.Visible;

                        _napsterRepository.GetPlayListsAsync()?.Data.ForEach(p =>
                            _mainViewModel.AllPlaylists.Add(new PlayListDTO()
                            {
                                Id = p.Id,
                                Name = p.Name,
                            }));
                        _mainViewModel.UpdatePlaylists();
                    });

                }
            }
        }

        private async Task<string> GetHTMLFromWebBrowser()
        {
            // MyWebBrowser.ViewSource();
            string html = null;
            return await MyWebBrowser.GetSourceAsync();
            //  return html;
        }

        private async void CollectionServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TypeApiService typeApiService = CollectionServices.SelectedItem as TypeApiService;

            if (typeApiService.IsLogin)
            {
                return;
            }


            _apiServ = ApiServiceConfigHoldder.GetApiServiceConfig
                (typeApiService.ServiceType);
            try
            {


                switch (typeApiService.ServiceType)
                {
                    case ServiceType.ITunes:
                        MyWebBrowser.Visibility = Visibility.Visible;
                        AllPlaylists.Visibility = Visibility.Hidden;

                        MyWebBrowser.Address = await _itunseLoginUser.GetAddressITunes();
                        break;
                    case ServiceType.Napster:
                        if (ApiServiceConfigHoldder.Tokens.ContainsKey(ServiceType.Napster))
                        {
                            MyWebBrowser.Visibility = Visibility.Hidden;
                            AllPlaylists.Visibility = Visibility.Visible;
                            break;
                        }

                        MyWebBrowser.Visibility = Visibility.Visible;
                        AllPlaylists.Visibility = Visibility.Hidden;
                        MyWebBrowser.Address = _apiServ.BaseUrl;
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            List<PlayListDTO> playLists = new List<PlayListDTO>();

            foreach (var item in _mainViewModel.AllPlaylists)
            {
                item.WidthPl = Width - ViewConfig.DeltaWidth;
                playLists.Add(item);
            }

            _mainViewModel.AllPlaylists = new ObservableCollection<PlayListDTO>(playLists);
        }


        private void ListBox_Selected(object sender, RoutedEventArgs e)
        {
            List<PlayListDTO> playLists = new List<PlayListDTO>();

            foreach (var item in _mainViewModel.AllPlaylists)
            {
                if (item.IsSelectedItem)
                {
                    item.IsVisible = Visibility.Visible;
                }
                else
                {
                    item.IsVisible = Visibility.Hidden;
                }

                playLists.Add(item);
            }

            _mainViewModel.AllPlaylists = new ObservableCollection<PlayListDTO>(playLists);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
