﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using ExportMusConv.UI.Config;
using ExportMusConv.UI.Config.ApiConfig;
using ExportMusConv.UI.Config.Common;
using Musconv.Entity.Napster;

namespace ExportMusConv.UI.Api.Napster
{
    public class NapsterRepository
    {
        private ApiServiceConfig _apiServ;

        public NapsterRepository()
        {
            _apiServ = ApiServiceConfigHoldder.GetApiServiceConfig
                (ServiceType.ITunes);

            if (_apiServ.UserData.Token == null)
            {
                if (ApiServiceConfigHoldder.Tokens.ContainsKey(ServiceType.Napster))
                {
                    _apiServ.UserData.Token = ApiServiceConfigHoldder.Tokens[ServiceType.Napster];
                }
            }

        }
        public void AddHeader(bool useToken, WebRequest request)
        {
            try
            {
                if (useToken)
                {
                    request.Headers.Add("Authorization: Bearer " + _apiServ.UserData.Token);
                }
                else
                {
                    //request.Headers.Add("apikey: " + AccessProperties.ClientId);
                }
            }
            catch
            {
            }
        }

        public DataResult<T>
            GetObjectAsync<T>(string url, bool useToken = false, string requestMethod = "GET")
        {
            DataResult<T> methodResult = new DataResult<T>();

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = requestMethod;


                AddHeader(useToken, request);
                WebResponse httpResponse = request.GetResponse();
                using (Stream stream = httpResponse.GetResponseStream())
                {
                    DataContractJsonSerializer serializer =
                        new DataContractJsonSerializer(typeof(T));
                    methodResult.Success = true;
                    methodResult.Data = (T)serializer.ReadObject(stream);

                }

                return methodResult;
            }
            catch (Exception e)
            {
                methodResult.Success = false;
                methodResult.Message = "You need clientId and clientSecret by Napster";

                return methodResult;
            }
        }

        public DataResult<IEnumerable<HttpWebResponse>>
            GetResponsesAsync(string url, int count, bool useToken = false)
        {
            DataResult<IEnumerable<HttpWebResponse>> methodResult =
                new DataResult<IEnumerable<HttpWebResponse>>();

            try
            {
                const int limit = 200;
                int offset = 0;
                List<HttpWebResponse> tasks = new List<HttpWebResponse>();
                while (offset < count)
                {
                    WebRequest request = WebRequest.Create(url + $"?offset={offset}&limit={ limit}");
                    request.Method = "GET";
                    AddHeader(useToken, request);
                    HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse();
                    tasks.Add(httpResponse);
                    offset += limit;
                }

                methodResult.Data = tasks;
                methodResult.Success = true;

                return methodResult;
            }
            catch
            {
                methodResult.Success = false;

                return methodResult;
            }
        }

        public async Task<Result> Login(string code)
        {
            Result methodResult = new Result();


            string url = "https://api.napster.com/oauth/access_token?" + $"client_id=" +
                $"{_apiServ.Client.ClientId}&client_secret={_apiServ.Client.ClientSecret}&response_type=code&grant_type=authorization_code&code=" + code;
            DataResult<NapsterData> root =
               GetObjectAsync<NapsterData>(url, requestMethod: "POST");

            if (!root.Success)
            {
                methodResult.Message = root.Message;

                return methodResult;
            }

            SetAccessProperties(root.Data);
            methodResult.Success = true;

            return methodResult;
        }

        private  void SetAccessProperties(NapsterData root)
        {
            _apiServ.UserData.Token = root.AccessToken;
       
        }

        public void SetAcces(Stream stream)
        {
            using (stream)
            {
                DataContractJsonSerializer serializer =
                    new DataContractJsonSerializer(typeof(NapsterData));

                NapsterData root = (NapsterData)serializer.ReadObject(stream);

                _apiServ.UserData.Token = root.AccessToken;
                ApiServiceConfigHoldder.Tokens.Add(ServiceType.Napster, root.AccessToken);
            }
        }

        public DataResult<List<Playlist>> GetPlayListsAsync()
        {
            DataResult<List<Playlist>> methodResult =
                new DataResult<List<Playlist>>();

            try
            {
                string url = $"https://api.napster.com/v2.2/me/library/playlists";
                DataResult<PlayListsRootObject> root = GetObjectAsync<PlayListsRootObject>(url, true);

                if (!root.Success)
                {
                    return methodResult;
                }
                root.Data.Playlists.ForEach(p => p.Description = p.Description
                .Replace("\n", "").Replace("\t", ""));
                methodResult.Data = root.Data.Playlists;
                methodResult.Success = true;

                return methodResult;
            }
            catch
            {
                methodResult.Success = false;

                return methodResult;
            }
        }

        public DataResult<List<Playlist>> GetTopPlayListsAsync()
        {
            DataResult<List<Playlist>> methodResult =
                new DataResult<List<Playlist>>();

            try
            {
                string url = $"https://api.napster.com/v2.2/me/library/playlists";
                DataResult<PlayListsRootObject> root = GetObjectAsync<PlayListsRootObject>(url, true);

                if (!root.Success)
                {
                    return methodResult;
                }
                root.Data.Playlists.ForEach(p => p.Description = p.Description
           .Replace("\n", "").Replace("\t", ""));
                methodResult.Data = root.Data.Playlists;
                methodResult.Success = true;

                return methodResult;
            }
            catch
            {
                methodResult.Success = false;

                return methodResult;
            }
        }

        public DataResult<List<Track>> GetPlayListTracksAsync(string playListId, int trackCount, bool isToken = false)
        {
            DataResult<List<Track>> methodResult =
                new DataResult<List<Track>>();
            try
            {
                List<Track> tracks = new List<Track>();
                string url = $"https://api.napster.com/v2.2/me/library/playlists/{playListId}/tracks";

                if (!isToken)
                {
                    url = $"https://api.napster.com/v2.0/playlists/{playListId}/tracks?apikey={_apiServ.Client.ClientId}&limit=200";
                }
                DataResult<IEnumerable<HttpWebResponse>> responses =
                      GetResponsesAsync(url, trackCount, isToken);


                if (!responses.Success)
                {
                    methodResult.Message = responses.Message;

                    return methodResult;
                }

                foreach (HttpWebResponse response in responses.Data)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        DataContractJsonSerializer serializer =
                            new DataContractJsonSerializer(typeof(TrackRootobject));
                        TrackRootobject obj = (TrackRootobject)serializer.ReadObject(stream);
                        tracks.AddRange(obj.Tracks);
                    }
                }

                methodResult.Success = true;
                methodResult.Data = tracks;

                return methodResult;
            }
            catch
            {
                methodResult.Success = false;

                return methodResult;
            }
        }

    }
}
