﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public class HRef
    {
        [DataMember(Name = "href")]
        public string Href { get; set; }
    }
}
