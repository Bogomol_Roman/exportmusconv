﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public class BaseItem : HRef
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "links")]
        public AlbumLinks Links { get; set; }
    }
}
