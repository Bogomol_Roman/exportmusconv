﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class TrackRootobject
    {
        [DataMember(Name = "meta")]
        public Meta Meta { get; set; }

        [DataMember(Name = "tracks")]
        public Track[] Tracks { get; set; }
    }
}
