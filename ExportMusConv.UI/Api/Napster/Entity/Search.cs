﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class Search
    {
        [DataMember(Name = "query")]
        public string Query { get; set; }   

        [DataMember(Name = "data")]
        public SearchData Data { get; set; }

        [DataMember(Name = "order")]
        public string[] Order { get; set; }
    }
}
