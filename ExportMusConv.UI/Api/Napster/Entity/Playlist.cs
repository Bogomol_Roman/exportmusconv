﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class Playlist : BaseItem
    {
        [DataMember(Name = "modified")]
        public string Modified { get; set; }

        [DataMember(Name = "trackCount")]
        public int TrackCount { get; set; }

        [DataMember(Name = "privacy")]
        public string Privacy { get; set; }


        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "favoriteCount")]
        public int FavoriteCount { get; set; }

        [DataMember(Name = "freePlayCompliant")]
        public bool FreePlayCompliant { get; set; }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
