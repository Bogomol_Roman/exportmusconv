﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class SearchRootobject
    {
        [DataMember(Name = "meta")]
        public Meta Meta { get; set; }

        [DataMember(Name = "search")]
        public Search Search { get; set; }
    }
}
