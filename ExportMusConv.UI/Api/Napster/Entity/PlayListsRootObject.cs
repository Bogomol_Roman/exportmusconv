﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{

    [DataContract]
    public sealed class PlayListsRootObject
    {
        [DataMember(Name = "playlists")]
        public List<Playlist> Playlists { get; set; } = new List<Playlist>();
    }
}
