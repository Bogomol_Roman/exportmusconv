﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class Track : BaseItem
    {
        [DataMember(Name = "index")]
        public int Index { get; set; }

        [DataMember(Name = "disc")]
        public int Disc { get; set; }

        [DataMember(Name = "playbackSeconds")]
        public int PlaybackSeconds { get; set; }

        [DataMember(Name = "isExplicit")]
        public bool IsExplicit { get; set; }

        [DataMember(Name = "isStreamable")]
        public bool IsStreamable { get; set; }

        [DataMember(Name = "isrc")]
        public string Isrc { get; set; }

        [DataMember(Name = "shortcut")]
        public string Shortcut { get; set; }

        [DataMember(Name = "blurbs")]
        public object[] Blurbs { get; set; }

        [DataMember(Name = "artistId")]
        public string ArtistId { get; set; }

        [DataMember(Name = "artistName")]
        public string ArtistName { get; set; }

        [DataMember(Name = "albumName")]
        public string AlbumName { get; set; }

        [DataMember(Name = "formats")]
        public Format[] Formats { get; set; }

        [DataMember(Name = "albumId")]
        public string AlbumId { get; set; }

        //[DataMember(Name = "contributors")]
        //public Contributors Contributors { get; set; }

        [DataMember(Name = "previewURL")]
        public string PreviewURL { get; set; }

        // Used for deduping tracks in the playlist editor.
        public bool IsSelected { get; set; } = true;

        public override string ToString()
        {
            return $"{ArtistName} - {Name}";
        }
    }
}
