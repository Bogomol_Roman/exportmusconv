﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Musconv.Entity.Napster
{
    [DataContract]
    public sealed class SearchData
    {
        [DataMember(Name = "tracks")]
        public IEnumerable<Track> Tracks { get; set; } = new List<Track>();


        [DataMember(Name = "playlists")]
        public IEnumerable<Playlist> Playlists { get; set; } = new List<Playlist>();
    }
}
