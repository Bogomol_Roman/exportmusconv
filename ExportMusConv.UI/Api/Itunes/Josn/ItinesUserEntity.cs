﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExportMusConv.UI.Itunes.Josn
{
   public  class ItinesUserEntity
    {
        [JsonProperty("musicUserToken")]
        public string Token { get; set; }
    }

    public static class HolderItunes
    {
        public static CookieContainer CookieContainer { get; set; }

    }
}
