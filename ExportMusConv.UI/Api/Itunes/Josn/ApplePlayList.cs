﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExportMusConv.UI.Itunes.Josn
{
    public class ApplePlayList
    {
        [JsonProperty("data")] public List<ApplePlayListDatum> Data { get; set; }
    }
  
    public class ApplePlayListDatum 
    {
        public Relationships Relationships { get; set; }
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("href")] public string Href { get; set; }


    }


    public class Attributes
    {
        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("description")] public string Description { get; set; }
    }

    public class Relationships2 : Relationships
    {
        [JsonProperty("tracks")] public new Tracks2 Tracks { get; set; }
    }

    public class Relationships
    {
        [JsonProperty("tracks")] public Tracks Tracks { get; set; }
    }

    public class Tracks
    {
        [JsonProperty("data")] public List<Datum> Data { get; set; }
    }

    public class Datum
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }
    }

    public class CreatePlayList 
    {
        public CreatePlayList(Attributes _Attributes, Relationships _Relationships)
        {
            Attributes = _Attributes;
            Relationships = _Relationships;
        }

        [JsonProperty("attributes")] public Attributes Attributes { get; set; }

        [JsonProperty("relationships")] public Relationships Relationships { get; set; }
    }

    public class Tracks2 : Tracks
    {
        [JsonProperty("data")] public new List<TracksDatum> Data { get; set; }

        [JsonProperty("next")] public string Next { get; set; }

        [JsonProperty("href")] public string Href { get; set; }
    }

    public class TracksDatum : Datum
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("attributes")] public FluffyAttributes Attributes { get; set; }
    }

    public class FluffyAttributes
    {
        [JsonProperty("url")] public string Url { get; set; }

        [JsonProperty("genreNames")] public List<string> GenreNames { get; set; }

        [JsonProperty("artwork")] public Artwork Artwork { get; set; }

        [JsonProperty("playParams")] public PlayParams PlayParams { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("albumName")] public string AlbumName { get; set; }

        [JsonProperty("artistName")] public string ArtistName { get; set; }

        [JsonProperty("durationInMillis")] public long DurationInMillis { get; set; }

        [JsonProperty("trackNumber")] public long TrackNumber { get; set; }
    }




    public class ApplePlayListInfo
    {
        [JsonProperty("data")] public List<Datum2> Data { get; set; }
    }








    public class Datum2 : Datum
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("attributes")] public Attributes2 Attributes { get; set; }
    }

    public class Attributes2 : Attributes
    {
        [JsonProperty("playParams")] public PlayParams PlayParams { get; set; }

        [JsonProperty("artwork")] public Artwork Artwork { get; set; }

        [JsonProperty("canEdit")] public bool CanEdit { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public new Description Description { get; set; }
    }

    public class Artwork
    {
        [JsonProperty("width")] public long? Width { get; set; }

        [JsonProperty("height")] public long? Height { get; set; }

        [JsonProperty("url")] public string Url { get; set; }
    }

    public class Description
    {
        [JsonProperty("standard")] public string Standard { get; set; }
    }

    public class PlayParams
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("kind")] public string Kind { get; set; }

        [JsonProperty("isLibrary")] public bool IsLibrary { get; set; }
    }

    public class AppleSearchResult
    {
        [JsonProperty("results")] public Results Results { get; set; }
    }

    public class Results
    {
        [JsonProperty("artists")] public Artists Artists { get; set; }

        [JsonProperty("songs")] public Songs Songs { get; set; }

        [JsonProperty("albums")] public Albums Albums { get; set; }
    }

    public class Albums
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("data")] public List<AlbumsDatum> Data { get; set; }
    }

    public class AlbumsDatum
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("attributes")] public PurpleAttributes Attributes { get; set; }
    }

    public class PurpleAttributes
    {
        [JsonProperty("canEdit")] public bool CanEdit { get; set; }

        [JsonProperty("artwork")] public Artwork Artwork { get; set; }

        [JsonProperty("artistName")] public string ArtistName { get; set; }

        [JsonProperty("isSingle")] public bool IsSingle { get; set; }

        [JsonProperty("url")] public string Url { get; set; }

        [JsonProperty("isComplete")] public bool IsComplete { get; set; }

        [JsonProperty("genreNames")] public List<string> GenreNames { get; set; }

        [JsonProperty("trackCount")] public long TrackCount { get; set; }

        [JsonProperty("isMasteredForItunes")] public bool IsMasteredForItunes { get; set; }

        [JsonProperty("releaseDate")] public DateTimeOffset ReleaseDate { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("recordLabel")] public string RecordLabel { get; set; }

        [JsonProperty("copyright")] public string Copyright { get; set; }

        [JsonProperty("playParams")] public PlayParams PlayParams { get; set; }

        [JsonProperty("editorialNotes", NullValueHandling = NullValueHandling.Ignore)]
        public EditorialNotes EditorialNotes { get; set; }

        [JsonProperty("contentRating", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentRating { get; set; }
    }


    public class EditorialNotes
    {
        [JsonProperty("standard")] public string Standard { get; set; }

        [JsonProperty("short")] public string Short { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
    }

    public class Artists
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("data")] public List<ArtistsDatum> Data { get; set; }
    }

    public class ArtistsDatum
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("attributes")] public FluffyAttributes Attributes { get; set; }
    }


    public class Songs
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("next")] public string Next { get; set; }

        [JsonProperty("data")] public List<SongsDatum> Data { get; set; }
    }

    public class SongsDatum : Datum
    {
        [JsonProperty("href")] public string Href { get; set; }

        [JsonProperty("attributes")] public TentacledAttributes Attributes { get; set; }
    }


    public class TentacledAttributes
    {
        [JsonProperty("previews")] public List<Preview> Previews { get; set; }

        [JsonProperty("artwork")] public Artwork Artwork { get; set; }

        [JsonProperty("artistName")] public string ArtistName { get; set; }

        [JsonProperty("url")] public string Url { get; set; }

        [JsonProperty("discNumber")] public long DiscNumber { get; set; }

        [JsonProperty("genreNames")] public List<string> GenreNames { get; set; }

        [JsonProperty("durationInMillis")] public long DurationInMillis { get; set; }

        [JsonProperty("releaseDate")] public DateTimeOffset ReleaseDate { get; set; }

        [JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("isrc")] public string Isrc { get; set; }

        [JsonProperty("albumName")] public string AlbumName { get; set; }

        [JsonProperty("playParams")] public PlayParams PlayParams { get; set; }

        [JsonProperty("trackNumber")] public long TrackNumber { get; set; }

        [JsonProperty("composerName", NullValueHandling = NullValueHandling.Ignore)]
        public string ComposerName { get; set; }
    }

    public class Preview
    {
        [JsonProperty("url")] public string Url { get; set; }
    }
}
