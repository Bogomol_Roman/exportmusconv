﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using ExportMusConv.UI.Config;
using ExportMusConv.UI.Config.ApiConfig;
using ExportMusConv.UI.Entity.DTO;
using ExportMusConv.UI.Itunes.Josn;
using Newtonsoft.Json;

namespace ExportMusConv.UI.Itunes
{
    public class ItunseLoginUser
    {
        private readonly ApiServiceConfig _apiServiceConfig;

        public async Task<bool> CreatePlaylist(string resultKey, List<TrackDTO> tracks)
        {
            var uri = new Uri($"https://api.music.apple.com/v1/me/library/playlists");
            var parameters = new Dictionary<string, string>();
            var headers = new Dictionary<string, string>
            {
                {"music-user-token", _apiServiceConfig.UserData.Token},
                {"authorization", $" Bearer {_apiServiceConfig.Token}"}
            };
            Relationships relationships = new Relationships();
            relationships.Tracks = new Tracks();
            relationships.Tracks.Data = new List<Datum>();

            foreach (var track in tracks)
            {
                AppleSearchResult appleSearchResult =
                    await SearchTrack(track.Name);
                SongsDatum music = appleSearchResult.Results?.Songs?.Data?.FirstOrDefault();

                if (music == null)
                {
                    continue;
                }

                relationships.Tracks.Data.Add(new Datum()
                {
                    Id = music.Id,
                    Type = music.Type
                }
                );
            }


            var atrAttributes = new Attributes { Name = resultKey, Description = "" };
            CreatePlayList createPlayList = new CreatePlayList(atrAttributes, relationships);

            return await Post(uri.OriginalString, headers, createPlayList);
        }

        public async Task<AppleSearchResult> SearchTrack(string title)
        {
            title = HttpUtility.UrlEncode(title);
            var uri = new Uri($"https://api.music.apple.com/v1/catalog/ua/search?term={title}&types=songs&limit=20");
            var parameters = new Dictionary<string, string>();

            var headers = new Dictionary<string, string>
            {
                {"music-user-token", _apiServiceConfig.UserData.Token},
                {"authorization", $" Bearer {_apiServiceConfig.Token}"}
            };

            return await GetObjectAsync<AppleSearchResult>(uri.OriginalString, headers);
        }

        public async Task<ApplePlayListInfo> GetPlayLists()
        {
            var uri = new Uri($"https://api.music.apple.com/v1/me/library/playlists");


            var headers = new Dictionary<string, string>
            {
                {"music-user-token", _apiServiceConfig.UserData.Token},
                {"authorization", $" Bearer {_apiServiceConfig.Token}"}
            };

            return await GetObjectAsync<ApplePlayListInfo>(uri.OriginalString, headers);
        }

        public async Task<ApplePlayList> GetApplePlayList()
        {
            int id = 0;
            var uri = new Uri($"https://api.music.apple.com/v1/me/library/playlists/{id}?include=tracks");


            var headers = new Dictionary<string, string>
            {
                {"music-user-token", _apiServiceConfig.UserData.Token},
                {"authorization", $" Bearer {_apiServiceConfig.Token}"},
                {"Accept-Encoding", "gzip, deflate, br"}

            };
            try
            {
                ApplePlayList main = await GetObjectAsync<ApplePlayList>(uri.OriginalString, headers);

                var next = new Uri(
                    $"https://api.music.apple.com/v1/me/library/playlists/{_apiServiceConfig.UserData.Id}/tracks?offset=100");
                while (true)
                    try
                    {
                        Tracks2 offset = await GetObjectAsync<Tracks2>(next.OriginalString, headers);


                        if (offset == null) return main;

                        main.Data.LastOrDefault()?.Relationships.Tracks.Data
                            .AddRange(offset.Data ?? throw new InvalidOperationException());
                        if (!string.IsNullOrEmpty(offset.Next))
                            next = new Uri("https://api.music.apple.com" + offset.Next);
                        else
                            return main;
                    }
                    catch (Exception x)
                    {
                        return main;
                    }
            }
            catch
            {
                return null;
            }
        }
        public ItunseLoginUser()
        {
            _apiServiceConfig =
                ApiServiceConfigHoldder.GetApiServiceConfig(ServiceType.ITunes);

            if (ApiServiceConfigHoldder.Tokens.ContainsKey(ServiceType.ITunes))
            {
                _apiServiceConfig.UserData.Token = ApiServiceConfigHoldder.Tokens[ServiceType.ITunes];
            }

        }
        public async Task<string> GetMusicUserTokenAsync(string id)
        {
            return
                  $"https://buy.itunes.apple.com/commerce/account/getMusicUserToken?liteSessionId={id}&jwtToken={_apiServiceConfig.Token}";

            var parameters = new Dictionary<string, string>();
            var headers = new Dictionary<string, string>();
            //{
            //    {"cache-control", "no-cache"},
            //    //{"Accept", "application/json, text/javascript, */*; q=0.01"},
            //    {"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"}
            ///   };

            //ItinesUserEntity token = await GetObjectAsync<ItinesUserEntity>(uri.OriginalString, headers);

            //ApiServiceConfig.UserData.Token = token?.Token;

            //return token?.Token;
        }

        public async Task<string> GetAddressITunes()
        {

            return await ApiAction(
                new Uri(_apiServiceConfig.BaseUrl),
                "POST",
                _apiServiceConfig.Parametrs.Data,
                _apiServiceConfig.Headers.Data,
                true);
        }

        public async Task<string> ApiAction(Uri uri, string method, Dictionary<string, string> parameters,
            Dictionary<string, string> sHeader, bool isnonjson)
        {
            var request = WebRequest.Create(uri);
            request.Method = method;

            var content = new FormUrlEncodedContent(parameters);
            HttpClient client = new HttpClient();

            foreach (var v in sHeader.ToList())
            {

                client.DefaultRequestHeaders.Add(v.Key, v.Value);
            }

            HttpResponseMessage hhHttpResponseMessage = await
                client.PostAsync(uri, content);

            return hhHttpResponseMessage.RequestMessage.RequestUri.AbsoluteUri;
        }

        public async Task<T> GetObjectAsync<T>(string url, Dictionary<string, string> sHeader
            , bool useToken = false, string requestMethod = "GET")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = requestMethod;
            //  request.CookieContainer = _apiServiceConfig.UserData.CookieContainer;
            //request.Timeout = 20000;
            WebResponse httpResponse = null;


            //  request.Connection = "keep-alive";
            try
            {
                foreach (var item in sHeader)
                {
                    request.Headers.Add(item.Key, item.Value);
                }


                httpResponse = await request.GetResponseAsync();

            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }


            if (httpResponse != null)
            {
                using (Stream stream = httpResponse.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        string res = sr.ReadToEnd();
                        return JsonConvert.DeserializeObject<T>(res);
                    }

                }
            }

            return default(T);
        }

        public async Task<bool> Post(string url, Dictionary<string, string> sHeader, object ob)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            foreach (var item in sHeader)
            {
                httpWebRequest.Headers.Add(item.Key, item.Value);
            }

            string json = JsonConvert.SerializeObject(ob);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                WebResponse httpResponse = await httpWebRequest.GetResponseAsync();
                using (Stream stream = httpResponse.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        string res = sr.ReadToEnd();
                        return true;
                    }

                }
            }
            catch
            {
                return false;
            }


        }
    }
}
