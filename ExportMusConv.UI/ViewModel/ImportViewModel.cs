﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ExportMusConv.UI.Config.Holder;
using ExportMusConv.UI.Entity.DTO;

namespace ExportMusConv.UI.ViewModel
{
    public class ImportViewModel : BasePropertyChanged
    {
        private ObservableCollection<ImportFile> _importFiles;
        private string _step;
        private string _description;
        private static ImportViewModel _importViewModel;


        public ImportViewModel()
        {
            _importFiles = new ObservableCollection<ImportFile>
                (ImportMethodHolder.ImportFiles);
            Step = "Step 1";
            Description = " : Select your import method";
            ImportViewModel._importViewModel = this;
        }

        public string Step
        {
            get => _step;
            set
            {
                _step = value;
                OnPropertyChanged(nameof(Step));
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public static ImportViewModel GetINstanse()
        {
            return _importViewModel;
        }

        public ObservableCollection<ImportFile> ImportFiles
        {
            get => _importFiles;
            set
            {
                _importFiles = value;
                OnPropertyChanged(nameof(ImportFiles));
            }
        }
    }
}
