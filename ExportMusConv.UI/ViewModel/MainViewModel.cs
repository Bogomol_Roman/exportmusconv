﻿using ExportMusConv.UI.Config;
using ExportMusConv.UI.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using ExportMusConv.UI.Api.Napster;
using ExportMusConv.UI.Config.Common;
using ExportMusConv.UI.Config.Holder;
using ExportMusConv.UI.Export;
using ExportMusConv.UI.Export.Abs;
using ExportMusConv.UI.Itunes;
using ExportMusConv.UI.Itunes.Josn;
using ExportMusConv.UI.View;
using ExportMusConv.UI.View.UserControl;
using Musconv.Entity.Napster;
using Application = System.Windows.Application;

namespace ExportMusConv.UI.ViewModel
{
    public class MainViewModel : BasePropertyChanged
    {
        private ObservableCollection<PlayListDTO> _allplaylists;
        private double _widthListBoxItem;
        private ICommand _export;
        private ICommand _exportInFile;
        private ObservableCollection<TypeFile> _typeFiles;
        private ObservableCollection<TypeApiService> _collectionServices;
        private ObservableCollection<TrackDTO> _tracks;
        private ICommand _import;
        private ItunseLoginUser _itunseLoginUser;

        private NapsterRepository _napsterRepository;

        private static MainViewModel _mainViewModel;

        public static MainViewModel GetInstanse()
        {
            return _mainViewModel;
        }

        public MainViewModel()
        {
            _mainViewModel = this;
            _tracks = new ObservableCollection<TrackDTO>();
            _itunseLoginUser = new ItunseLoginUser();
            _napsterRepository = new NapsterRepository();
            _widthListBoxItem = ((System.Windows.Controls.Panel)
               Application.Current.MainWindow.Content).ActualWidth;
            AllPlaylists = new ObservableCollection<PlayListDTO>();
            _typeFiles = new ObservableCollection<TypeFile>();
            _collectionServices = new ObservableCollection<TypeApiService>
                (ApiServiceHolder.TypeFiles);
        }

        public ICommand Import
        {
            get => _import ?? ( _import = new RelayCommand
                       (async o =>
                   {
                       new ImportFiles().ShowDialog();

                       if (ServiceItemControl.IsReload)
                       {
                        ApplePlayListInfo playListInfos =  await _itunseLoginUser.GetPlayLists();
                         

                           foreach (var p in playListInfos.Data)
                           {
                               if (_mainViewModel.AllPlaylists.Any(s => s.Id == p.Id))
                               {
                                   continue;
                               }

                               _mainViewModel.AllPlaylists.Add(new PlayListDTO()
                               {
                                   Id = p.Id,
                                   Name = p.Attributes.Name,
                               });
                           }
                       }
                   }));
        }
  
        public ICommand ExportInFile
        {
            get
            {
                return _exportInFile ?? (_exportInFile = new RelayCommand(ob =>
             {
                 IExport export = new ExportCsv();
                 string extantion = ".csv";

                 switch (TypeFileHolder.CurrentFileType)
                 {
                     case FileType.M3u:
                         extantion = ".m3u";
                         export = new ExportM3U();
                         break;
                     case FileType.M3u8:
                         extantion = ".m3u8";
                         export = new ExportM3U();
                         break;
                     case FileType.Txt:
                         extantion = ".txt";
                         export = new ExportTxt();
                         break;
                     case FileType.Json:
                         extantion = ".json";
                         export = new ExportJson();
                         break;

                 }

                 using (var folderBrowserDialog = new FolderBrowserDialog())
                 {
                     DialogResult dialogResult = folderBrowserDialog.ShowDialog();
                     if (dialogResult == System.Windows.Forms.DialogResult.OK)
                     {
                     }

                     string fileName = "Tracks";
                     try
                     {
                         int id = 1;
                         while (File.Exists($"{folderBrowserDialog.SelectedPath}\\{fileName}{extantion}"))
                         {
                             fileName = $"{fileName} ({id})";
                             id++;
                         }

                         string path = Path.Combine(folderBrowserDialog.SelectedPath, $"{ fileName}{extantion}");
                         Result result = new Result();

                         result = export.ExportFile(Tracks.ToList(), path);


                         if (result.Success)
                         {
                             System.Windows.MessageBox.Show(" was saved successfully!",
                                    "Success!", MessageBoxButton.OK, MessageBoxImage.None);
                         }
                         else
                         {
                             System.Windows.MessageBox.Show(result.Message,
                                    "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                         }
                     }
                     catch
                     {

                     }
                 }
             }));
            }
        }

        public ICommand Export
        {
            get
            {
                return _export ?? (_export = new RelayCommand(o =>
                {

                    ExportWindow exportWindow = new ExportWindow(this);

                    exportWindow.ShowDialog();
                }));
            }
        }

        public ObservableCollection<TrackDTO> Tracks
        {
            get => _tracks;
            set
            {
                _tracks = value;
                OnPropertyChanged(nameof(Tracks));
            }
        }
        public ObservableCollection<TypeApiService> CollectionServices
        {
            get
            {
                return _collectionServices;
            }
            set
            {
                _collectionServices = value;
                OnPropertyChanged(nameof(CollectionServices));
            }
        }

        public ObservableCollection<TypeFile> TypeFiles
        {
            get
            {
                return _typeFiles;
            }
            set
            {
                _typeFiles = value;
                OnPropertyChanged(nameof(TypeFiles));
            }
        }

        public ObservableCollection<PlayListDTO> AllPlaylists
        {
            get
            {
                return _allplaylists;
            }
            set
            {
                _allplaylists = value;
                OnPropertyChanged(nameof(AllPlaylists));
            }
        }

        //public void UpdateTypeFiles()
        //{
        //    foreach (var typeFile in TypeFiles)
        //    {
        //        typeFile.PropertyChanged += TypeFile_PropertyChanged;
        //    }
        //}

        //private void TypeFile_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{



        //}

        public void UpdatePlaylists()
        {
            foreach (var playlist in AllPlaylists)
            {
                playlist.PropertyChanged += Playlist_PropertyChanged; ;
            }

        }

        private void Playlist_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is PlayListDTO itemData)
            {
                if (e.PropertyName == nameof(itemData.IsSelectedItem))
                {
                    UpdateSelectedItems();
                }
            }
        }

        private async void UpdateSelectedItems()
        {
            await Task.Run(() =>
            {
                foreach (var playlist in AllPlaylists)
                {
                    if (playlist.IsSelectedItem)
                    {
                        if (playlist.Tracks == null || !playlist.Tracks.Any())
                        {
                            playlist.Tracks = _napsterRepository.GetPlayListTracksAsync(playlist.Id, 100, true)
                                .Data.Select(p => new TrackDTO()
                                {
                                    Album = p.AlbumName,
                                    Artist = p.ArtistName,
                                    Duration = p.Disc.ToString(),
                                    Id = p.Id,
                                    Url = p.PreviewURL,
                                    Name = p.Name

                                }).ToList();

                        }

                    }
                }
            });
        }

        public double WidthPl
        {
            get
            {
                return _widthListBoxItem;
            }
            set
            {
                _widthListBoxItem = value;

                OnPropertyChanged(nameof(WidthPl));
            }
        }
    }
}
